﻿namespace DVP3_VAS_Ex1_GroceryList
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.grpBox = new System.Windows.Forms.GroupBox();
            this.haveTxtBx = new System.Windows.Forms.TextBox();
            this.haveLbl = new System.Windows.Forms.Label();
            this.addBtn = new System.Windows.Forms.Button();
            this.addLbl = new System.Windows.Forms.Label();
            this.needTxtBx = new System.Windows.Forms.TextBox();
            this.needLB = new System.Windows.Forms.ListBox();
            this.haveLB = new System.Windows.Forms.ListBox();
            this.switchToHaveBtn = new System.Windows.Forms.Button();
            this.switchToNeedBtn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.removeBtn = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            this.grpBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(758, 40);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(64, 36);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(165, 38);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(165, 38);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // grpBox
            // 
            this.grpBox.Controls.Add(this.haveTxtBx);
            this.grpBox.Controls.Add(this.haveLbl);
            this.grpBox.Controls.Add(this.addBtn);
            this.grpBox.Controls.Add(this.addLbl);
            this.grpBox.Controls.Add(this.needTxtBx);
            this.grpBox.Location = new System.Drawing.Point(57, 60);
            this.grpBox.Name = "grpBox";
            this.grpBox.Size = new System.Drawing.Size(645, 195);
            this.grpBox.TabIndex = 1;
            this.grpBox.TabStop = false;
            this.grpBox.Text = "Grocery List Input";
            // 
            // haveTxtBx
            // 
            this.haveTxtBx.Location = new System.Drawing.Point(210, 100);
            this.haveTxtBx.Name = "haveTxtBx";
            this.haveTxtBx.Size = new System.Drawing.Size(203, 31);
            this.haveTxtBx.TabIndex = 1;
            // 
            // haveLbl
            // 
            this.haveLbl.AutoSize = true;
            this.haveLbl.Location = new System.Drawing.Point(38, 103);
            this.haveLbl.Name = "haveLbl";
            this.haveLbl.Size = new System.Drawing.Size(166, 25);
            this.haveLbl.TabIndex = 3;
            this.haveLbl.Text = "Add to have list:";
            // 
            // addBtn
            // 
            this.addBtn.Location = new System.Drawing.Point(449, 57);
            this.addBtn.Name = "addBtn";
            this.addBtn.Size = new System.Drawing.Size(171, 74);
            this.addBtn.TabIndex = 2;
            this.addBtn.Text = "Add";
            this.addBtn.UseVisualStyleBackColor = true;
            this.addBtn.Click += new System.EventHandler(this.addBtn_Click);
            // 
            // addLbl
            // 
            this.addLbl.AutoSize = true;
            this.addLbl.Location = new System.Drawing.Point(38, 60);
            this.addLbl.Name = "addLbl";
            this.addLbl.Size = new System.Drawing.Size(167, 25);
            this.addLbl.TabIndex = 1;
            this.addLbl.Text = "Add to need list:";
            // 
            // needTxtBx
            // 
            this.needTxtBx.Location = new System.Drawing.Point(211, 57);
            this.needTxtBx.Name = "needTxtBx";
            this.needTxtBx.Size = new System.Drawing.Size(202, 31);
            this.needTxtBx.TabIndex = 0;
            // 
            // needLB
            // 
            this.needLB.FormattingEnabled = true;
            this.needLB.ItemHeight = 25;
            this.needLB.Location = new System.Drawing.Point(36, 309);
            this.needLB.Name = "needLB";
            this.needLB.Size = new System.Drawing.Size(260, 379);
            this.needLB.TabIndex = 2;
            // 
            // haveLB
            // 
            this.haveLB.FormattingEnabled = true;
            this.haveLB.ItemHeight = 25;
            this.haveLB.Location = new System.Drawing.Point(448, 309);
            this.haveLB.Name = "haveLB";
            this.haveLB.Size = new System.Drawing.Size(274, 379);
            this.haveLB.TabIndex = 3;
            // 
            // switchToHaveBtn
            // 
            this.switchToHaveBtn.Location = new System.Drawing.Point(316, 309);
            this.switchToHaveBtn.Name = "switchToHaveBtn";
            this.switchToHaveBtn.Size = new System.Drawing.Size(117, 99);
            this.switchToHaveBtn.TabIndex = 4;
            this.switchToHaveBtn.Text = "Switch to have";
            this.switchToHaveBtn.UseVisualStyleBackColor = true;
            this.switchToHaveBtn.Click += new System.EventHandler(this.switchToHaveBtn_Click);
            // 
            // switchToNeedBtn
            // 
            this.switchToNeedBtn.Location = new System.Drawing.Point(316, 581);
            this.switchToNeedBtn.Name = "switchToNeedBtn";
            this.switchToNeedBtn.Size = new System.Drawing.Size(117, 107);
            this.switchToNeedBtn.TabIndex = 5;
            this.switchToNeedBtn.Text = "Switch to need";
            this.switchToNeedBtn.UseVisualStyleBackColor = true;
            this.switchToNeedBtn.Click += new System.EventHandler(this.switchToNeedBtn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(31, 281);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(109, 25);
            this.label1.TabIndex = 6;
            this.label1.Text = "Need List:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(443, 281);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(108, 25);
            this.label2.TabIndex = 7;
            this.label2.Text = "Have List:";
            // 
            // removeBtn
            // 
            this.removeBtn.Location = new System.Drawing.Point(316, 476);
            this.removeBtn.Name = "removeBtn";
            this.removeBtn.Size = new System.Drawing.Size(117, 41);
            this.removeBtn.TabIndex = 8;
            this.removeBtn.Text = "Remove";
            this.removeBtn.UseVisualStyleBackColor = true;
            this.removeBtn.Click += new System.EventHandler(this.removeBtn_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(758, 739);
            this.Controls.Add(this.removeBtn);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.switchToNeedBtn);
            this.Controls.Add(this.switchToHaveBtn);
            this.Controls.Add(this.haveLB);
            this.Controls.Add(this.needLB);
            this.Controls.Add(this.grpBox);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.grpBox.ResumeLayout(false);
            this.grpBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.GroupBox grpBox;
        private System.Windows.Forms.TextBox haveTxtBx;
        private System.Windows.Forms.Label haveLbl;
        private System.Windows.Forms.Button addBtn;
        private System.Windows.Forms.Label addLbl;
        private System.Windows.Forms.TextBox needTxtBx;
        private System.Windows.Forms.ListBox needLB;
        private System.Windows.Forms.ListBox haveLB;
        private System.Windows.Forms.Button switchToHaveBtn;
        private System.Windows.Forms.Button switchToNeedBtn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button removeBtn;
    }
}

