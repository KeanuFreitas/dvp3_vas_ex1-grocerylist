﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace DVP3_VAS_Ex1_GroceryList
{

    // Keanu Freitas
    // Project & Portfolio
    // Robin Alarcon
    // 11/17/2016
    // VAS
    // Exercise 1
    // Grocery List

    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Stream savingFile;
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Filter = "text files (*.txt)|*txt";
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                if ((savingFile = saveFileDialog1.OpenFile()) != null)
                {
                    using (TextWriter _file = new StreamWriter(savingFile))
                    {
                        _file.WriteLine("Need List:\n");
                        foreach (var needListItems in needLB.Items)
                        {
                            _file.WriteLine(needListItems.ToString());
                        }

                        _file.WriteLine("--------------------------------");
                        _file.WriteLine("Have List:\n");
                        foreach (var haveListItems in haveLB.Items)
                        {
                            _file.WriteLine(haveListItems.ToString());
                        }
                    }
                    savingFile.Close();
                }
            }
        }

        private void addBtn_Click(object sender, EventArgs e)
        {
            if(needTxtBx.Text == "" && haveTxtBx.Text == "")
            {
                MessageBox.Show("There is no input! Please try again:");
            }
            if (needTxtBx.Text != "")
            {
                needLB.Items.Add(needTxtBx.Text);
            }
            if (haveTxtBx.Text != "")
            {
                haveLB.Items.Add(haveTxtBx.Text);
            }
            needTxtBx.Clear();
            haveTxtBx.Clear();
        }

        private void switchToHaveBtn_Click(object sender, EventArgs e)
        {
            for (int intCountNeed = needLB.SelectedItems.Count -1; intCountNeed >= 0; intCountNeed--)
            {
                haveLB.Items.Add(needLB.SelectedItems[intCountNeed]);
                needLB.Items.Remove(needLB.SelectedItems[intCountNeed]);
            }
        }

        private void switchToNeedBtn_Click(object sender, EventArgs e)
        {
            for (int intCountHave = haveLB.SelectedItems.Count -1; intCountHave >= 0; intCountHave--)
            {
                needLB.Items.Add(haveLB.SelectedItems[intCountHave]);
                haveLB.Items.Remove(haveLB.SelectedItems[intCountHave]);
            }
        }

        private void removeBtn_Click(object sender, EventArgs e)
        {
            while(needLB.SelectedItems.Count > 0)
            {
                needLB.Items.Remove(needLB.SelectedItems[0]);
            }
            while (haveLB.SelectedItems.Count > 0)
            {
                haveLB.Items.Remove(haveLB.SelectedItems[0]);
            }
        }
    }
}
